# Spigot

Spigot a small collection minimalist graphical interfaces for Sporth, 
implemented as a Sporth plugin.
It is powered by glfw3 and [runt](https://www.github.com/paulbatchelor/runt).

Typical graphical user interfaces for music are by and large "graphics-first".
That is to say, the program spawns a GUI *then* starts the audio engine. 
With Spigot, the audio starts first and then the GUI spawns from Sporth. 
Spigot is able to be reloaded, allowing it to be used in live coding 
situations.

